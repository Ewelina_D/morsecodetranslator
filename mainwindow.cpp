#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    translator("../MorseCodeTranslator/alphabet.txt"),
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Morse Code Translator");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_openButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), ".", tr("Text files (*.txt)") );

    if ( fileName.isEmpty() )
        return;

    QFile file( fileName );
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in( &file );
    ui->inputText->clear();
    while (!in.atEnd())
    {
        QString line = in.readLine();
        ui->inputText->append( line );
    }
    file.close();
}

void MainWindow::on_codeButton_clicked()
{
    ui->outputText->clear();
    QString input = ui->inputText->toPlainText();       //get text from QTextEdit
    QString output = translator.code(input);
    ui->outputText->append(output);
}

void MainWindow::on_saveButton_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this);
    QFile file(fileName);

    if(file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream output(&file);
        output << ui->outputText->toPlainText();
        file.flush();
        file.close();
    }
}

void MainWindow::on_decodeButton_clicked()
{
    ui->outputText->clear();            // clear previous QTextBrowser content
    QString input = ui->inputText->toPlainText();
    QString output = translator.decode(input);
    ui->outputText->append(output);
}

void MainWindow::resizeEvent(QResizeEvent *)
{
    ui->inputText->resize(this->width()-185, this->height()*0.42);
    ui->outputText->resize(this->width()-185, this->height()*0.42);

    ui->codeButton->setGeometry(ui->inputText->x()+0.5*ui->inputText->width()-110, ui->inputText->height()+20, 100,30);
    ui->decodeButton->setGeometry(ui->inputText->x()+0.5*ui->inputText->width()+10, ui->inputText->height()+20, 100,30);

    ui->outputText->setGeometry(165, ui->inputText->height()+60, this->width()-185, this->height()*0.42);
}

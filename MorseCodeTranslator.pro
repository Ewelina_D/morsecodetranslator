#-------------------------------------------------
#
# Project created by QtCreator 2015-10-27T18:37:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MorseCodeTranslator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    translator.cpp

HEADERS  += mainwindow.h \
    translator.h

FORMS    += mainwindow.ui

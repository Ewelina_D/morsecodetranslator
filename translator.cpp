#include "translator.h"

Translator::Translator(QString path)
{
    filePath = path;
    loadAlphabet();
}

QString Translator::code(QString text)
{
    text = text.toUpper();
    QStringList wordList = text.split(" ");         //cut QString into single words
    QString translatedText="";

    foreach(QString item, wordList)
    {
        //for each character in the word I check if this char is in my alphabet
        for(int i=0; i<item.length(); i++)
        {
            if(alphabet.contains(QChar(item[i])))
            {
                translatedText = translatedText + alphabet[QChar(item[i])] + " ";
            }
        }
        translatedText = translatedText + " ";
    }
    return translatedText;
}

QString Translator::decode(QString text)
{
    text=text.trimmed();
    QStringList wordList = text.split("  ");
    QString translatedText="";

    foreach(QString wordItem, wordList)
    {
        QStringList charList = wordItem.split(" ");
        foreach (QString charItem, charList)
        {
            if(alphabet.key(charItem) == "")
            {
                int ret;
                QMessageBox msgBox;
                msgBox.setText("There is illegal characters in the input text.\n You can use only: '.', '-' and ' ' (space)");
                ret = msgBox.exec();
                return "";
            }
            else
                translatedText = translatedText + alphabet.key(charItem);
        }
        translatedText += " ";
    }
    return translatedText;
}

void Translator::loadAlphabet()
{
    QFile file( filePath );
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        int ret;
        QMessageBox msgBox;
        msgBox.setText("There is no text file with alphabet: MorseCodeTranslator/alphabet.txt");
        ret = msgBox.exec();
        return;
    }
    else
    {
        QTextStream in( &file );
        QString key, value;
        while (!in.atEnd())
        {
            in.operator >>(key);
            in.operator >>(value);
            alphabet.insert(key,value);
        }
        alphabet.insert("\n", "");
    }
}

#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <QTextStream>
#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QDialog>

class MainWindow;

class Translator
{
public:

    Translator(QString path);
    QString code(QString text);
    QString decode(QString text);

private:
    QString filePath;
    QMap<QString, QString> alphabet;
    void loadAlphabet();        //load alphabet from txt file
};

#endif // TRANSLATOR_H

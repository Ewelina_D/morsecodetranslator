#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QTextStream>
#include <QMap>

#include "translator.h"

namespace Ui {
class MainWindow;
}
class Translator;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Translator translator;

private slots:
    void on_openButton_clicked();
    void on_codeButton_clicked();
    void on_saveButton_clicked();
    void on_decodeButton_clicked();

private:
    Ui::MainWindow *ui;

protected:
     virtual void resizeEvent(QResizeEvent *);

};

#endif // MAINWINDOW_H
